FROM maven:3.6.3 AS development
COPY . .
RUN mvn package -Dmaven.test.skip

FROM eclipse-temurin:17-jre-jammy
ARG JAR_FILE=target/*.jar
COPY --from=development ${JAR_FILE} app.jar
CMD ["java", "-jar", "/app.jar"]